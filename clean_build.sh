#!/bin/sh
find platforms/android/app/src/main/res/* -type d | grep -Ev "values|xml" | xargs rm -rf
rm platforms/android/app/src/main/assets/www/icons/app.png
rm platforms/android/app/src/main/assets/www/icons/README
rm platforms/android/app/src/main/assets/www/js/libs/README
rm platforms/android/app/src/main/assets/www/css/README

exit 0
