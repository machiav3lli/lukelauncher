cordova.define("com.luke.widget.Widget", function(require, exports, module) {
/*global cordova, module*/

module.exports = {
    
    create: function (top,bottom,full,allow_scroll_in_widget,translation, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "create", [top,bottom,full,allow_scroll_in_widget,translation]);
    },
    
    destroy: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "destroy");
    },
    
    hide: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "hide");
    },
    
    show: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "show");
    },
    
    padding: function (top,bottom,full, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "padding", [top,bottom,full]);
    } , 
    
    transparency: function (percent, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "transparency", [percent]);
    } ,
    
    save: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "save");
    },   
    
    load: function (load_string, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "load", [load_string]);
    }, 
    
    add_widget_settings: function (translation,successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "add_widget_settings");
    },
       
    add_widget_settings_state: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "Widget", "add_widget_settings_state");
    } 
};

});
